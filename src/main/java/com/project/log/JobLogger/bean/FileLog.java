package com.project.log.JobLogger.bean;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Configuration
public class FileLog {
	
	private File logFile = null;
	private FileHandler fh = null;
	private Logger logger;
	
	@Value("${log.file.path}")
	private String filePath;
	@Value("log.file.name")
	private String fileName;
	
	@Bean
	public Logger fileStartLog() throws Exception{
		System.out.println("Start: "+filePath+" - "+fileName);
		logFile = new File(filePath + fileName);
		if (!logFile.exists()) {			
				logFile.createNewFile();			
		}
		logger = Logger.getLogger("FileLog");
		fh = new FileHandler(filePath + fileName,true);
		logger.addHandler(fh);
		
		return logger;

	}
	
	
	public void message(String messageText) {
		try {
			logger = fileStartLog();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.log(Level.INFO, messageText);
	}
	
	public void warning(String messageText) {
		logger.log(Level.WARNING, messageText);
		
	}
	
	public void error(String messageText) {
		logger.log(Level.SEVERE, messageText);
		
	}

}
