package com.project.log.JobLogger.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Configuration
public class DataBaseLog {
	
	Connection connection = null;
	Properties connectionProps = new Properties();
	Statement stmt = null;

	@Value("${log.database.userName}")
	private String userName;
	
	@Value("${log.database.password}")
	private String password;
	
	@Value("${log.database.dbms}")
	private String dbms;
	
	@Value("${log.database.serverName}")
	private String serverName;
	
	@Value("${log.database.portNumber}")
	private String portNumber;
	
	@Value("${log.database.db}")
	private String db;
	
	@Bean
	public Statement dataBaseConnectionLog() throws SQLException {
		connectionProps.put("user", userName);
		connectionProps.put("password", password);
		
		connection = DriverManager.getConnection("jdbc:" + dbms + "://" + serverName+ ":" + portNumber + "/"+db, connectionProps);
		return stmt = connection.createStatement();
	}
	
	public void message(String messageText) {
		try {
			stmt = dataBaseConnectionLog();
			stmt.executeUpdate("insert into Log_Values values('" + messageText + "', '1')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void warning(String messageText) {
		try {
			stmt = dataBaseConnectionLog();
			stmt.executeUpdate("insert into Log_Values values('" + messageText + "', '2')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void error(String messageText) {
		try {
			stmt = dataBaseConnectionLog();
			stmt.executeUpdate("insert into Log_Values values('" + messageText + "', '3')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
