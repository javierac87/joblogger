package com.project.log.JobLogger.bean;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

@Component
public class ConsoleLog {
	
	private Logger logger;
	private ConsoleHandler ch = null;
	
	public ConsoleLog() {
		logger = Logger.getLogger("ConsoleLog");
		ch = new ConsoleHandler();
		logger.addHandler(ch);
	}
	
	public void message(String messageText) {
		logger.log(Level.INFO, messageText);
	}
	
	public void warning(String messageText) {
		logger.log(Level.WARNING, messageText);
		
	}
	
	public void error(String messageText) {
		logger.log(Level.SEVERE, messageText);
		
	}

}
