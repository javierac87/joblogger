package com.project.log.JobLogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobLoggerApplication.class, args);
	}
}
