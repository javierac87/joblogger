package com.project.log.JobLogger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.log.JobLogger.bean.DataBaseLog;
import com.project.log.JobLogger.bean.FileLog;

@RestController
public class MainController {
	
	@Autowired
	private FileLog fileLog;

	@Autowired
	private DataBaseLog dataBaseLog;
	
	@RequestMapping("/file")
	public String logFile() {
		
		long mili = System.currentTimeMillis();
		fileLog.message("File print: "+mili);
		
		return "print Log";
		
	}
	
	
	@GetMapping("/database")
	public String dataBaseFile() {
		
		long mili = System.currentTimeMillis();
		dataBaseLog.warning("database print: "+mili);
		
		return "print Log";
		
	}

}
