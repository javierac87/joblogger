package com.project.log.JobLogger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.log.JobLogger.bean.ConsoleLog;
import com.project.log.JobLogger.bean.DataBaseLog;
import com.project.log.JobLogger.bean.FileLog;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootApplication()
@TestPropertySource("classpath:application.properties")
public class LogTest {

	@Autowired
	private FileLog fileLog;
	@Autowired
	private DataBaseLog dataBaseLog;
	@Autowired
	private ConsoleLog consoleLog;

	@Before
	public void loadTest() {

	}

	@Test
	public void testFile() {
		String message = "Advertencia del test";
		fileLog.warning(message);

	}

	@Test
	public void testConsole() {
		String message = "Mensaje del test";
		consoleLog.message(message);

	}

	@Test
	public void testDataBase() {
		String message = "Error al insertar";
		dataBaseLog.error(message);

	}
}
