#BASE DE DATOS USADA. MYSQL
# CREAR BASE DATOS bd_logger

#CARGAR ARCHIVO SQL CON EL SCRIPT DE LA TABLA
#ARCHIVO PROPERTIES DE LA APLICACION CUENTA CON LAS CREDENCIALES, CONFIGURAR
PROPIEDADES:
log.database.userName
log.database.password=
log.database.dbms
log.database.serverName
log.database.portNumber
log.database.db=bd_logger?autoReconnect=true&useSSL=false
----------------------------------------------------

#CONFIGURAR RUTA Y NOMBRE DEL ARCHIVO LOG, AMBOS PARÁMETROS SE ENCUENTRAN EN EL ARCHIVO APPLICATION.PROPERTIES
log.file.path
log.file.name